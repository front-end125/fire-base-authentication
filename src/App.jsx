import { Routes, Route } from 'react-router-dom';
// import Home from './routes/home/home.component';
import Navigation from './routes/navigation/navigation.component';
import SignIn from './routes/sign-in/sign-in.component';
import Landing from './routes/landing-page/landing.component'

const Shop = () => {
  return <h1> I am a Shop</h1>
}


const App = () => {
  return (
    <Routes>
      <Route path='/' element={<Navigation />}>
        <Route index element={<Landing />} />
        <Route path='shop' element={ <Shop/>} />
        <Route path='sign-in' element={ <SignIn/>} />
      </Route>
      
    </Routes>
  );
};

export default App;
