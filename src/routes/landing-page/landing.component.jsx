import { Fragment } from 'react';

import './landing.styles.scss';

const Landing = () => {
    return (
        <Fragment>
            <div className='landing'>
                <div className='landing-container'>
                        <form action="" className='seach-form'>
                            <input type="text" placeholder="Search.." name="search" className='text-input'/>
                            <button type="submit" className="btn-search" >Search</button>
                        </form>
                    <div className='calendar-container'>
                        <div className='calendar'>
                            <h1> Event 1 </h1>
                            <h5>Lorem ipsum, dolor sit amet consectetur adipisicing elit.</h5>
                        </div>
                        <div className='calendar'>
                            <h1> Event 2 </h1>
                            <p>Lorem ipsum dolor sit amet</p>
                        </div>
                        <div className='calendar'>
                            <h1> Event 3 </h1>
                            <p> Lorem ipsum dolor sit amet </p>
                        </div>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}
export default Landing;