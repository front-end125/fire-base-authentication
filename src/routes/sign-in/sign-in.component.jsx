import { Fragment, useEffect } from 'react';

import { getRedirectResult } from 'firebase/auth';
import SignUpForm from '../../components/singUpForm/singUpForm.component';


import { 
    singInWithGooglePopup , 
    createUserDocumentFromAuth, 
    singInWithGoogleRedirect,
    auth
} from '../../utils/firebase/firebase.utils';

import './sign-in.style.scss'



const SignIn = () => {

    useEffect(() => {
        async function getAuth(){
            const response = await getRedirectResult(auth);
            if(response){
                const userDocRef=  createUserDocumentFromAuth(response.user);
            }
        }
        getAuth();
    },[]);

    const loqGoogleUser = async ()=> {
        const {user} = await singInWithGooglePopup();
        const userDocRef=  createUserDocumentFromAuth(user);
    };

    return (
        <Fragment>
            <div className='singin'>
                <div className=' singin-container'>
                    <div className='form-login-container'>
                        <SignUpForm/>
                    </div>
                    <div className="vl"></div>
                    <div className='profile-login-container'>
                        <button onClick={ loqGoogleUser} className='btn-profile-login'> Sign in with Google Popup</button>
                        <button onClick={ singInWithGoogleRedirect} className='btn-profile-login'> Sign in with Google Redirect</button>
                    </div>                    
                </div>                
            </div>            
        </Fragment>
    );
};

export default SignIn;