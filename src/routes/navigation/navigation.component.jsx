import { Fragment } from 'react';
import { Outlet, Link} from 'react-router-dom';

import Footer from '../../components/footer/footer.component'
import { ReactComponent as BibleLogo} from '../../assets/preferences-system.svg';
import './navigation.style.scss'

const Navigation = () => {
    return (
      <Fragment>
        <div className='navigation'>
            <Link className='logo-container' to='/'>
                {/* <div> Logo </div> */}
                <BibleLogo className='logo' />
            </Link>
            
            <div className='nav-links-container'>
                <Link className='nav-link' to='/shop'>
                    SHOP
                </Link>
                <Link className='nav-link' to='/sign-in'>
                    SingIn
                </Link>
            </div>
        </div>
        <Outlet/>
        <Footer/>
      </Fragment>
    );
  }

export default Navigation;