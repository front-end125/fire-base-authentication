import { Fragment } from 'react';

import './footer.styles.scss'

const Footer = () => {
    return (
        <Fragment>
            <div className='footer-container'>
                <div className='footer'>
                    <div className='footer-contact'>
                        <h1 className='footer-title'> contact</h1>
                        <p> contcat us on !</p>
                    </div>
                    <div className='footer-social-media'>
                        <h1 className='footer-title'> Social media</h1>
                        <p> facebook</p>
                    </div>
                    <div className='footer-link'>
                        <h1 className='footer-title'> link </h1>
                        <p> About</p>
                        <p></p>
                    </div>
                </div>
            </div>
        </Fragment>
    );
}

export default Footer;