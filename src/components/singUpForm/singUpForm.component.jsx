import { Fragment, useState } from 'react';

import {
    createAuthUserWithEmailAndPassword,
    createUserDocumentFromAuth
} from '../../utils/firebase/firebase.utils'

import FormInput from '../form-input/form-input.component'


const defaultFormFields = {
    displayName : '',
    email:'',
    password:'',
    confirmPassword: ''
}

const SignUpForm = () => {

    const [formFields, setFormFileds] = useState(defaultFormFields);
    const {displayName, email,password,confirmPassword}=formFields;

    console.log(formFields);

    const resetFormFields= () => {
        setFormFileds(defaultFormFields);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();


        if(password !== confirmPassword){
            alert( "Mismatch password!");
            return;
        }

        try {
            const response=  await createAuthUserWithEmailAndPassword(email,password);
            if(response){
                const userRefDoc=  await createUserDocumentFromAuth(response.user, {displayName});
                console.log(userRefDoc)
                resetFormFields();
            }
        } catch (error) {
            if(error.code==='auth/email-already-in-use'){
                alert('Cannot create user, email already in use');
            }else{
                if(error.code==='auth/weak-password'){
                    alert('Password should be at least 6 characters');
                }else{
                    console.log( " User creation encounter an error: " , error)
                }
                
            }
        }
       
        
    }

    const handleChange =(event) => {
        const {name, value} = event.target;

        setFormFileds({ ...formFields, [name]:value});
    }
    return (
        <Fragment>
            <div>
                <h1> Sign up with email and password </h1>
                <form onSubmit={handleSubmit}>
                    <FormInput label="displayName" type="text" name="displayName" required onChange={handleChange} value={displayName}  />

                    <FormInput label="email"  type="email" required  name="email" onChange={handleChange} value={email}/>

                    <FormInput label="password"  type="password" required name="password" onChange={handleChange} value={password}/>

                    <FormInput label="confirmPassword"  type="password" required name="confirmPassword" onChange={handleChange} value={confirmPassword}/>

                    <button type="submit" className="btn-sing-up" >Sign Up</button>
                </form> 
            </div>
        </Fragment>
    );
}

export default SignUpForm;