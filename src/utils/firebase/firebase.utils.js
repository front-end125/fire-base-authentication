import { initializeApp} from 'firebase/app';
import { 
  getAuth, 
  signInWithRedirect, 
  signInWithPopup,
  GoogleAuthProvider,
  createUserWithEmailAndPassword
} from 'firebase/auth';

import {
  getFirestore,
  doc,
  getDoc,
  setDoc
} from 'firebase/firestore';

const firebaseConfig = {
    apiKey: "AIzaSyAXudKr5_h7emyjs11YmZhAyQKYWPVJYx8",
    authDomain: "crwn-clothing-db-6c94b.firebaseapp.com",
    projectId: "crwn-clothing-db-6c94b",
    storageBucket: "crwn-clothing-db-6c94b.appspot.com",
    messagingSenderId: "389306520055",
    appId: "1:389306520055:web:ddd7a4be8ccc3976ff5ef2"
  };
  
  // Initialize Firebase
  const fireBaseApp = initializeApp(firebaseConfig);

  const googleProvider = new GoogleAuthProvider();

  googleProvider.setCustomParameters({
    prompt: " select_account"
  });

  export const auth = getAuth();
  export const singInWithGooglePopup = () => signInWithPopup(auth, googleProvider);
  export const singInWithGoogleRedirect = () => signInWithRedirect(auth, googleProvider);

  // Firestore intansiated

  export const db= getFirestore();

  export const createUserDocumentFromAuth = async (
    userAuth,
    additionalInformation = {}
    ) => {
        const userDocRef = doc(db, 'users', userAuth.uid); // check if user exist
        console.log(userDocRef);
        const usersSnapshot = await getDoc(userDocRef);
        console.log(usersSnapshot);
        console.log(usersSnapshot.exists());

        // check if exits

        // if does not exist 
        // Create doc into fire store   

        // return userDoc

        if(!usersSnapshot.exists()){
          const { displayName, email } = userAuth;

          const createdAt = new Date();

          try {
            await setDoc(userDocRef,{
                displayName,
                email,
                createdAt,
                ... additionalInformation
            });
          } catch (error) {
            console.log(' error creating the user '. error.message)
          }
        }

        return userDocRef;

  };


/*   Create user with Email and password.
  Implementation */

 export const createAuthUserWithEmailAndPassword  = async (email, password) => {
  if(!email || !password) return;
  return await   createUserWithEmailAndPassword(auth,email,password);
 }

